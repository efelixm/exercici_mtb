from django.db import models


class Rings(models.Model):
    rings = models.CharField(max_length=4)
    count = models.IntegerField(default=0)
    
    def __str__(self):              
        return self.rings 

    def as_json(self):
        return dict(rings=int(self.rings), count=self.count)
