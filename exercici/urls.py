from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('exercici.views',
    # Examples:
    # url(r'^$', 'exercici.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    #url(r'^$', 'home', name='home'),
    #url(r'^grafica/', 'grafica', name='grafica'),
    url(r'^$', 'grafica', name='grafica'),    
    url(r'^admin/', include(admin.site.urls)),

)
