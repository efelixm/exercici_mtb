# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import Context, Template
from exercici.models import Rings
import ast
import json


def home(request):
    """
    home page
    """
    return render_to_response('home.html')


def grafica(request):
    """
    bar chart page
    """
    # reading directly from json :S
    # with open("/var/www/html/demoproject/static/rings.json") as json_file:
    #    json_data = json.load(json_file)
    # data = json.dumps(json_data)

    # query from django. problems with unicode, this won't happen with p3k
    # a bit strange, but it works for now, keep working on html+js!! 
    chart_name = "Rings chart"
    y_axis_label = "count"
    dades = Rings.objects.all()
    dades_molonas = [ast.literal_eval(json.dumps(ob.as_json())) for ob in dades]

    return render_to_response('grafica.html', {'data': dades_molonas, 'chart_name': chart_name, 'y_axis_label': y_axis_label})

